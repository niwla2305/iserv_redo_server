import json
import re

from flask import *
from iservscrapping import Iserv

from app import app


def getconfig(mode):
    with open("settings.json", mode) as config_file:
        config_raw = config_file.read()
        configuration = json.loads(config_raw)
        return configuration


# read some config
base_url = getconfig("r")["base_url"]
plan_url = getconfig("r")["plan_url"]
user = getconfig("r")["user"]
password = getconfig("r")["password"]

print("Iserv Redo Server started.")

iserv = Iserv(base_url, user, password)


def update(schoolclass, ip):
    plan = []

    plan_raw = iserv.get_untis_substitution_plan(plan_url, schoolclass)

    for entry in plan_raw:
        entry_2_text = ""
        timeWrote = False
        for entry_2 in entry:
            if timeWrote is False:
                time = entry_2
                timeWrote = True
            else:
                entry_2_text = " " + entry_2_text + entry_2 + " "

        # Replace Shortcuts for subjects with their full names
        entry_2_text = re.sub(r"[→\s]MU[\s→]", " Musik ", entry_2_text)
        entry_2_text = re.sub(r"[→\s]EN[\s→]", " Englisch ", entry_2_text)
        entry_2_text = re.sub(r"[→\s]PO[\s→]", " Politik ", entry_2_text)
        entry_2_text = re.sub(r"[→\s]EN[\s→]", " Englisch ", entry_2_text)
        entry_2_text = re.sub(r"[→\s]DE[\s→]", " Deutsch ", entry_2_text)
        entry_2_text = re.sub(r"[→\s]PH[\s→]", " Physik ", entry_2_text)
        entry_2_text = re.sub(r"[→\s]SP[\s→]", " Sport ", entry_2_text)
        entry_2_text = re.sub(r"[→\s]KU[\s→]", " Kunst ", entry_2_text)
        entry_2_text = re.sub(r"[→\s]BI[\s→]", " Bio ", entry_2_text)
        entry_2_text = re.sub(r"[→\s]SN[\s→]", " Spanisch ", entry_2_text)
        entry_2_text = re.sub(r"[→\s]FR[\s→]", " Fränzösich ", entry_2_text)
        entry_2_text = re.sub(r"[→\s]LA[\s→]", " Latein ", entry_2_text)
        entry_2_text = re.sub(r"[→\s]MA[\s→]", " Mathe ", entry_2_text)
        entry_2_text = re.sub(r"[→\s]WN[\s→]", " Werte u. Normen ", entry_2_text)
        entry_2_text = re.sub(r"[→\s]RE[\s→]", " Religion ", entry_2_text)
        entry_2_text = re.sub(r"[→\s]---[\s→]", " Ausfall ", entry_2_text)

        # create a dict to append to the list of entrys (plan)
        tempdict = {'time': time, 'content': entry_2_text}
        plan.append(tempdict)
    session["plan"] = plan


@app.route('/<schoolclass>/')
def plan_for_class(schoolclass):
    update(schoolclass, session)
    return render_template('vertreter.html', plan=session["plan"], title=schoolclass, schoolclass=schoolclass)


@app.route('/')
def index():
    return render_template('select.html', darkmode=getconfig("r")['darkmode'])


@app.route('/', methods=['POST'])
def index_redirect():
    if request.form.get('class'):
        return redirect("/" + request.form.get('class') + "/")


@app.route('/manifest.json')
def manifest():
    return send_from_directory('static', 'manifest.json')


# @app.route('/sw.js')
# def service_worker():
#     response = make_response(send_from_directory('static', 'sw.js'))
#     response.headers['Cache-Control'] = 'no-cache'
#     return response

